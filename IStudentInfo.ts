//Interface declaration
interface IStudentInfo{
    Name: string,
    Age: number,
    Phone: number,
    Language: string
}

export {IStudentInfo as StudentInfo}
