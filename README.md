# Introduction to type script

Code developed during the course [Typescript Fundamentals](https://www.udemy.com/course/ts-fundamental/)

## Requirements

* **[Node.js](https://nodejs.org/en/)**
* **[TypeScript](https://www.typescriptlang.org/download)**
* **[Visual Studio Code](https://code.visualstudio.com/)**

## Execution

* node + name_file.js

