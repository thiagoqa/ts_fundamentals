/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 01-type.ts
* Executação node app.js
*/

import {StudentInfo} from './IStudentInfo'
import Students from './Students'

// Part 2
console.log("Hello ExecuteAutomation");

// Part 3
let a:boolean = true
let b:string = "Oliveira"
var k:number = 123456798
console.log("Hello " + a)

// Part 4
//enum declaration
enum LanguagesKnown {English, Hindi, Tamil}

//Declaring student
let student:StudentInfo = {
    Name: "Thiago",
    Age: 30,
    Phone: 944522141,
    Language: LanguagesKnown[LanguagesKnown.Tamil]
}

//List of students
let studentsList:StudentInfo[] = [
    {Name: "Thiago", Age:30, Phone: 232656055, Language: LanguagesKnown[LanguagesKnown.English]},
    {Name: "Batima", Age:32, Phone: 211212121, Language: LanguagesKnown[LanguagesKnown.Hindi]},
]

let studentsListG:Array<StudentInfo> = [
    {Name: "Thiago", Age:30, Phone: 232656055, Language: LanguagesKnown[LanguagesKnown.English]},
    {Name: "Batima", Age:32, Phone: 211212121, Language: LanguagesKnown[LanguagesKnown.Hindi]},
]
// Pushing a student into the array (List of students)
// studentsList.push(student);

for (var index = 0; index < studentsList.length; index++) {
    var element = studentsList[index];
    console.log("Age:" + element.Age + "with Name:" + element.Name + " has Phone:" + element.Phone + " language:" + element.Language);
}

// Part 5
// Function declaration
function GetStudentsList(students:StudentInfo[]){
    students.forEach(element => {
        console.log("Age:" + element.Age + "with Name:" + element.Name + " has Phone:" + element.Phone + " language:" + element.Language);
    })
}

GetStudentsList(studentsList);

// Rest parameters
function GetNumbers(...nums: number[]){
    nums.forEach(element =>{
        console.log("Numbers: " + element)
    })
}

GetNumbers(1,2,3,4,5,6);

// Default parameters
function GetInfo(info:string = "Happy"){
    console.log(info);
}

GetInfo();

// Part 6
// Anonymous function
let StudentName = function (fName:string,sName:string){
    return sName + "..." + fName;
}
console.log(StudentName("Function", "Anonymous"));

// Arrow function
let StudentFullName = (fName:string,sName:string) => {return sName + "..." + fName;}

console.log(StudentFullName("Function", "Arrow"));

//Part 8

let s = new Students("Oliveira", "Thiago")
console.log(s.GetFullName());

//Part 9
// Generics
function GetStudentsListGenerics(students:Array<StudentInfo>){
    students.forEach(element => {
        console.log("Age:" + element.Age + "with Name:" + element.Name + " has Phone:" + element.Phone + " language:" + element.Language);
    })
}
GetStudentsListGenerics(studentsListG)
